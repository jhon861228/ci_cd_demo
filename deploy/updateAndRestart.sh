#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
cd /home/ec2-user/ci_cd_demo/

# clone the repo again
echo "Actualizando"
git pull

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ec2-user/node/bin:$PATH
#source /home/ec2-user/.nvm/nvm.sh

# stop the previous pm2
echo "Mata pm2"
pm2 kill

echo "Remover pm2"
#npm remove pm2


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
#sudo npm install pm2
# starting pm2 daemon
echo "Inicia PM2"
pm2 start
pm2 status

cd /home/ec2-user/ci_cd_demo

#install npm packages
echo "Running npm install"
sudo npm install

#Restart the node server
npm start
